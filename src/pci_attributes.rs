use crate::uevent::UEvent;
use serde::Serialize;
use std::collections::HashMap;
#[derive(Serialize, Default)]
pub struct PciAttributes {
    pub class: u32,
    pub subsystem_vendor: String,
    pub subsystem_device: String,
    pub dma_mask_bits: u8,
    pub vendor: String,
    pub device: String,
    pub enable: u8,
    pub revision: u16,
    pub irq: u8,
    pub local_cpus: u8,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub uevent: Option<UEvent>,
    pub resource: Vec<u64>,
    pub ari_enabled: u8,
    pub current_link_width: u8,
    pub max_link_width: u8,
    pub msi_bus: u8,
    pub consistent_dma_mask_bits: u8,
    pub secondary_bus_number: u8,
    pub subordinate_bus_number: u8,
    pub broken_parity_status: u8,
    pub d3cold_allowed: u8,
    pub numa_node: i8,
    pub modalias: String,
    pub current_link_speed: String,
    pub max_link_speed: String,
    pub driver_override: Option<String>,
    pub local_cpulist: String,
    pub aer_dev_fatal: String,
    pub aer_dev_nonfatal: String,
    pub aer_dev_correctable: String,
    #[serde(skip)]
    pub config: Vec<u8>,
    #[serde(flatten, skip_serializing_if = "HashMap::is_empty")]
    pub attributes: HashMap<String, String>,
}
