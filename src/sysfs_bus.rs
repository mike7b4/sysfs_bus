use crate::pci_attributes::PciAttributes;
use crate::uevent::UEvent;
use serde::Serialize;
use std::collections::HashMap;
use std::fmt;
use std::fs::*;
use std::os::unix::fs::MetadataExt;
use std::path::Path;
use std::str::FromStr;

/// Deserialized /sys/class/bus/usb to a Rust structure.
/// Note unknown fields are stored in raw format in the attributes: HashMap<String, String>
#[derive(Serialize, Default)]
pub struct UsbAttributes {
    pub num_endpoints: u8,
    pub num_configurations: u8,
    pub num_interfaces: u8,
    pub interface_class: u8,
    pub interface_number: u8,
    pub interface_protocol: u8,
    pub alternate_setting: u8,
    pub interface_subclass: u8,
    pub max_packet_size0: u16,
    pub supports_autosuspend: u8,
    pub authorized: u8,
    #[serde(skip_serializing_if = "String::is_empty")]
    pub modalias: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id_product: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id_vendor: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub bus_num: Option<u8>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dev_num: Option<u8>,
    #[serde(skip_serializing_if = "String::is_empty")]
    pub product: String,
    #[serde(skip_serializing_if = "String::is_empty")]
    pub manufacturer: String,
    #[serde(skip_serializing_if = "String::is_empty")]
    pub serial: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub uevent: Option<UEvent>,
    #[serde(skip)]
    pub descriptors: Vec<u8>,
    #[serde(flatten, skip_serializing_if = "HashMap::is_empty")]
    pub attributes: HashMap<String, String>,
}

fn u8_from_attr(value: &str) -> Option<u8> {
    if value.starts_with("0x") {
        u8::from_str_radix(&value[2..], 16).ok()?;
    }
    u8::from_str_radix(value, 10).ok()
}

/// Deserialized generic SysFs /sys/class/* to a Rust structure.
/// Note most fields are stored in raw format in the HashMap<String, String>
#[derive(Serialize, Default)]
pub struct GenericClassAttributes {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub uevent: Option<UEvent>,
    #[serde(flatten, skip_serializing_if = "HashMap::is_empty")]
    pub attributes: HashMap<String, String>,
}

type SysPath = String;
pub type UsbDevices = HashMap<SysPath, UsbAttributes>;
pub type PciDevices = HashMap<SysPath, PciAttributes>;
pub type DmiInfo = HashMap<SysPath, GenericClassAttributes>;
pub type ThermalInfo = HashMap<SysPath, GenericClassAttributes>;

/// Deserialized SysFs bus/class types supported by this crate
pub enum SysFsType {
    BusUSB(UsbDevices),
    BusPCI(PciDevices),
    ClassDMI(DmiInfo),
    ClassThermal(ThermalInfo),
}

impl fmt::Display for SysFsType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use crate::sysfs_bus::SysFsType::*;
        write!(
            f,
            "{}",
            match self {
                BusUSB(_) => "USB",
                BusPCI(_) => "PCI",
                ClassDMI(_) => "DMI",
                ClassThermal(_) => "Thermal",
            }
        )
    }
}

impl From<&mut SysFsType> for &Path {
    fn from(kind: &mut SysFsType) -> Self {
        use crate::sysfs_bus::SysFsType::*;
        match kind {
            BusUSB(_) => Path::new("/sys/bus/usb/devices/"),
            BusPCI(_) => Path::new("/sys/bus/pci/devices/"),
            ClassDMI(_) => Path::new("/sys/class/dmi/"),
            ClassThermal(_) => Path::new("/sys/class/thermal"),
        }
    }
}

/// SysFs builds up struct based on what kind of SysFs type.
#[derive(Default, Serialize)]
pub struct SysFs {}
impl SysFs {
    fn usb_device_attributes(
        attrs: &mut UsbAttributes,
        key: &str,
        value: Vec<u8>,
    ) -> Result<(), std::io::Error> {
        if key == "descriptors" {
            attrs.descriptors = value;
            return Ok(());
        }

        let value = if let Ok(value) = String::from_utf8(value) {
            value.trim().to_string()
        } else {
            log::warn!("Could not decode {} as string.", key);
            return Ok(());
        };
        match &key[0..] {
            "busnum" => {
                attrs.bus_num = u8_from_attr(&value);
            }
            "devnum" => {
                attrs.dev_num = u8_from_attr(&value);
            }
            "supports_autosuspend" => attrs.supports_autosuspend = value.parse().unwrap(),
            "authorized" => attrs.authorized = value.parse().unwrap(),
            "uevent" => {
                attrs.uevent = UEvent::from_str(&value).ok();
            }
            "bNumEndpoints" => attrs.num_endpoints = value.parse().unwrap(),
            "bNumInterfaces" => attrs.num_interfaces = value.parse().unwrap(),
            "bNumConfigurations" => attrs.num_configurations = value.parse().unwrap(),

            "bInterfaceNumber" => attrs.interface_number = u8::from_str_radix(&value, 16).unwrap(),
            "bInterfaceProtocol" => {
                attrs.interface_protocol = u8::from_str_radix(&value, 16).unwrap()
            }
            "bInterfaceClass" => attrs.interface_class = u8::from_str_radix(&value, 16).unwrap(),
            "bInterfaceSubClass" => {
                attrs.interface_subclass = u8::from_str_radix(&value, 16).unwrap()
            }
            "bAlternateSetting" => {
                attrs.alternate_setting = u8::from_str_radix(&value, 16).unwrap()
            }
            "bMaxPacketSize0" => attrs.max_packet_size0 = value.parse().unwrap(),
            "manufacturer" => attrs.manufacturer = value,
            "product" => attrs.product = value,
            "serial" => attrs.serial = value,
            _ => {
                attrs.attributes.insert(key.into(), value);
            }
        }

        Ok(())
    }

    /// Read path and pass it to anonymous function
    fn read_generic<F, P>(path: P, mut f: F) -> Result<(), std::io::Error>
    where
        F: FnMut(&str, Result<Vec<u8>, std::io::Error>),
        P: AsRef<Path>,
    {
        for entry in read_dir(path)? {
            if let Ok(entry) = entry {
                if metadata(entry.path())?.mode() & 0o400 != 0 && entry.path().is_file() {
                    if let Some(key) = entry.path().file_name() {
                        let key = &key.to_string_lossy();
                        match std::fs::read(&mut entry.path()) {
                            Ok(value) => {
                                f(&key, Ok(value));
                            }
                            Err(e) => {
                                f(&key, Err(e));
                            }
                        }
                    }
                }
            }
        }
        Ok(())
    }

    fn pci_attributes(
        attrs: &mut PciAttributes,
        key: &str,
        value: Vec<u8>,
    ) -> Result<(), std::io::Error> {
        if key == "config" {
            attrs.config = value;
            return Ok(());
        }
        let value = String::from_utf8_lossy(&value)
            .to_string()
            .trim()
            .to_string();
        match &key[0..] {
            "revision" => {
                attrs.revision = u16::from_str_radix(&value[2..], 16).unwrap();
            }
            "uevent" => {
                attrs.uevent = UEvent::from_str(&value).ok();
            }
            "enable" => attrs.enable = value.parse().unwrap(),
            "class" => attrs.class = u32::from_str_radix(&value[2..], 32).unwrap(),
            "numa_node" => attrs.numa_node = value.parse().unwrap(),
            "dma_mask_bits" => attrs.dma_mask_bits = value.parse().unwrap(),
            "current_link_width" => attrs.current_link_width = value.parse().unwrap(),
            "max_link_width" => attrs.max_link_width = value.parse().unwrap(),
            "consistent_dma_mask_bits" => attrs.consistent_dma_mask_bits = value.parse().unwrap(),
            "secondary_bus_number" => attrs.secondary_bus_number = value.parse().unwrap(),
            "subordinate_bus_number" => attrs.subordinate_bus_number = value.parse().unwrap(),
            "d3cold_allowed" => attrs.d3cold_allowed = value.parse().unwrap(),
            "driver_override" => {
                if value != "(null)" {
                    attrs.driver_override = Some(value);
                }
            }
            "broken_parity_status" => attrs.broken_parity_status = value.parse().unwrap(),
            "local_cpus" => {
                attrs.local_cpus = u8::from_str_radix(&value, 16).unwrap();
            }
            "msi_bus" => attrs.msi_bus = value.parse().unwrap(),
            "ari_enabled" => attrs.ari_enabled = value.parse().unwrap(),
            "irq" => attrs.irq = u8_from_attr(&value).unwrap(),
            "resource" => {
                let value = value.replace('\n', " ");
                for r in value.split(' ') {
                    attrs
                        .resource
                        .push(u64::from_str_radix(&r[2..], 16).unwrap());
                }
            }
            _ => {
                attrs.attributes.insert(key.into(), value);
            }
        }
        Ok(())
    }

    fn iterate(kind: &mut SysFsType) -> Result<(), std::io::Error> {
        let path: &Path = (kind).into();
        if path.is_dir() {
            for entry in read_dir(&path)? {
                if let Ok(entry) = entry {
                    if entry.path().is_dir() {
                        if let Ok(link) = read_link(entry.path()) {
                            let p = Path::new(&path).join(link).canonicalize();
                            if let Ok(ref p) = p {
                                let key: String =
                                    p.clone().into_os_string().to_string_lossy().into();
                                match kind {
                                    SysFsType::BusUSB(map) => {
                                        let mut attrs = UsbAttributes::default();
                                        Self::read_generic(p, |key, value| match value {
                                            Ok(value) => {
                                                Self::usb_device_attributes(&mut attrs, key, value)
                                                    .unwrap();
                                            }
                                            Err(e) => {
                                                log::warn!(
                                                    "Could not read read USB: {}/{} cause: {}",
                                                    p.to_string_lossy(),
                                                    key,
                                                    e
                                                );
                                            }
                                        })?;
                                        if !attrs.descriptors.is_empty() {
                                            map.insert(key, attrs);
                                        }
                                    }
                                    SysFsType::BusPCI(map) => {
                                        let mut attrs = PciAttributes::default();
                                        Self::read_generic(p, |key, value| match value {
                                            Ok(value) => {
                                                Self::pci_attributes(&mut attrs, key, value)
                                                    .unwrap();
                                            }
                                            Err(e) => {
                                                log::warn!(
                                                    "Could not read read pci: {}/{} cause: {}",
                                                    p.to_string_lossy(),
                                                    key,
                                                    e
                                                );
                                            }
                                        })?;
                                        map.insert(key, attrs);
                                    }
                                    SysFsType::ClassDMI(map) => {
                                        map.insert(key, Self::iterate_generic_attributes(p)?);
                                    }
                                    SysFsType::ClassThermal(map) => {
                                        map.insert(key, Self::iterate_generic_attributes(p)?);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        Ok(())
    }

    fn iterate_generic_attributes(p: &Path) -> Result<GenericClassAttributes, std::io::Error> {
        let mut inner = GenericClassAttributes::default();
        Self::read_generic(p, |key, value| match value {
            Ok(value) => {
                let value = String::from_utf8_lossy(&value)
                    .to_string()
                    .trim()
                    .to_string();
                match &key[0..] {
                    "uevent" => inner.uevent = UEvent::from_str(&value).ok(),
                    _ => {
                        inner.attributes.insert(key.to_string(), value);
                    }
                }
            }
            Err(e) => {
                log::warn!("Could not read {} key: '{}' cause: {}", p.display(), key, e);
            }
        })?;
        Ok(inner)
    }

    /// Get all USB devices found on system.
    pub fn usb_devices() -> Result<UsbDevices, std::io::Error> {
        let mut kind = SysFsType::BusUSB(UsbDevices::new());
        Self::iterate(&mut kind)?;
        match kind {
            SysFsType::BusUSB(usb) => Ok(usb),
            _ => panic!(""),
        }
    }

    /// Get all PCI devices found on system.
    pub fn pci_devices() -> Result<PciDevices, std::io::Error> {
        let mut kind = SysFsType::BusPCI(PciDevices::new());
        Self::iterate(&mut kind)?;
        match kind {
            SysFsType::BusPCI(pci) => Ok(pci),
            _ => panic!(""),
        }
    }

    /// Get DMI info
    pub fn dmi_info() -> Result<DmiInfo, std::io::Error> {
        let mut kind = SysFsType::ClassDMI(DmiInfo::new());
        Self::iterate(&mut kind)?;
        match kind {
            SysFsType::ClassDMI(dmi) => Ok(dmi),
            _ => panic!(""),
        }
    }

    /// Get all thermal devices on the system.
    pub fn thermal_info() -> Result<ThermalInfo, std::io::Error> {
        let mut kind = SysFsType::ClassThermal(ThermalInfo::new());
        Self::iterate(&mut kind)?;
        match kind {
            SysFsType::ClassThermal(temp) => Ok(temp),
            _ => panic!(""),
        }
    }
}
